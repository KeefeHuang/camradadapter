"""
@package tauadapter
Python wrapper to control the execution of TAU coupled with preCICE
"""

from abc import ABCMeta, abstractmethod


class DataHandler:
    """ Abstract DataHandler base class that handles data to and from TAU\n

    Attributes:
        mesh_id (int): unique integer id identifying the specific mesh in precice interface
        data_id (int): unique integer id identifying the specific data type in precice interface
    """
    __metaclass__ = ABCMeta

    def __init__(self, mesh_id, data_id, precice_vertex_ids):
        self.mesh_id = mesh_id
        self.data_id = data_id
        self.precice_vertex_ids = precice_vertex_ids
        # Number of dimensions in problem
        self.length = len(self.precice_vertex_ids)

    @abstractmethod
    def write(self, iteration):
        """ Abstract function used to write data to preCICE

        Arguments:
            iteration (int): current coupled iteration as int
        """
        pass

    @abstractmethod
    def read(self, data, iteration):
        """ Abstract function used to read data from preCICE

        Arguments:
            data: data received from preCICE. This is in a 1D array of size (n x 3)
            iteration (int): current coupled iteration as int
        """
        pass

    @abstractmethod
    def is_scalar(self):
        """Function returns True if data type is scalar
        """
        pass
