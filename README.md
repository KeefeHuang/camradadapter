# Project Title

This is the CAMRAD-preCICE adapter, written to allow the coupling the CAMRAD CSD
solver to various other solid solvers. This solver is mainly designed to run 
loosely-coupled helicopter simulations but fully supports tightly coupled
simulationsn as well

## Getting Started

In order to get started, please ensure the following environmental variables are
set:
*  Ensure that the CAMRAD executable is in your PATH environmental variable.
*  Ensure that the {ROOT_DIRECTORY} of this project is in PYTHONPATH

These instructions will get you a copy of the project up and running on your 
local machine for development and testing purposes.

### Prerequisites

This project is mainly based in Python and has the following depencies:
* preCICE
* netCDF4
* yaml
* numpy
* scipy
* jsonobject

## Running the tests

To run tests on the base classes, enter {ROOT_DIRECTORY}/tests and run 
`python -m unittest discover`.

The unit tests for each solver are also available. Navigate to
{ROOT_DIRECTORY}/{SOLVER_TYPE}/tests and run
`python -m unittest discover`


## Test Examples

Please refer to the {ROOT_DIRECTORY}/Tutorials/ folder for various example codes.
There are currently two working tutorials Flap and Helicopter, which 
respectively show a  sample tightly- and loosely-coupled simulation between
TAU and CAMRAD.


## Authors

* **Huang Qunsheng** - *Initial work*


## Acknowledgments

* With thanks to Gerasimos Chourdakis and Amine Abdelmoula for their support and guidance in writing this code
