"""
@package tauadapter
Python wrapper to control the execution of TAU coupled with preCICE
"""
import logging
from camradadapter.datahandler import DataHandler


class ScalarHandler(DataHandler):
    """ ScalarHandler class. Reads force/moment data from fluid solver

    Attributes:
        precice_vertex_ids (list): list of unique integer ids identifying the specific vertices in
        the precice interface
        logger (logging.Logger): logger object used to log data
        length (int): number of nodes on interface (n) for current process
    """

    def __init__(self, mesh_id, data_id, precice_vertex_ids, data_names):
        """ Constructor for ForcesHandler class.

        Arguments:
            mesh_id (int): unique integer id identifying the specific mesh in precice interface
            data_id (list): unique integer id identifying the specific data type in precice
                interface
            precice_vertex_ids (ndarray): list of unique integer ids identifying the specific
                vertices in the precice interface
        """
        super(ScalarHandler, self).__init__(mesh_id, data_id, precice_vertex_ids)

        self.precice_vertex_ids = precice_vertex_ids
        # Number of dimensions in problem
        self.data_names = data_names

        # Number of nodes handled by ForceHandler
        self.length = len(self.precice_vertex_ids)

        self.logger = logging.getLogger("camrad.scalarhandler")

    def write(self, iteration):
        """ Implements datahandlers.DataHandler.read() abstract function. Write is not implemented
        for vectors in CAMRAD

        Arguments:
            displacement data is arranged as [x1, y1, z1, x2, y2, z2 ...]
            iteration (int): current coupled iteration as int
        """
        self.logger.error("Read is not implemented for Time for this module\n")

    def read(self, data, iteration):
        """ Reads force/moment data from preCICE. Write is called but not implemented
        for vectors in CAMRAD

        Arguments:
            data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            iteration (int): current coupled iteration as int
        """
        pass

    def is_scalar(self):
        """
        Returns:
             Returns false as this is a vector data type
        """
        return True
