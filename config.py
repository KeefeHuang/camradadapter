"""
@package tauadapter
Python wrapper to control the execution of TAU coupled with preCICE
"""
import jsonobject


class RotorInfo(jsonobject.JsonObject):
    """ RotorInfo class represents a jsonobject that stores the data from the rotorblade definition

    Attributes:
        radius (float): rotorblade radius as float
        nblade (int): number of rotorblades in simulation as int
        omega (float): rotational velocity of rotorblade in rad/s as float
        ashaft (float): rotor shaft angle of attack in degrees as float
        acant (float): rotor cant (roll) angle in degrees as float
        rprop (list): radial stations of XQC and ZQC properties as list of floats
        xqc (list): x-axis quarter chord offset from reference line in m as list of floats
        zqc (list): z-axis quarter chord offset from reference line in m as list of floats
        opaz (int):
        blade_spacing (list): azimuth angles (in degrees) of each blade in rotor as list of floats
        redge (list): radial coordinates of blade panel edges as list of floats, length of
            number of panels + 1
        rpos (list) : radial coordinates of CAMRAD blade sensors as list of floats
    """

    radius = jsonobject.FloatProperty(default=0)
    nblade = jsonobject.IntegerProperty()
    omega = jsonobject.FloatProperty()
    ashaft = jsonobject.FloatProperty()
    acant = jsonobject.FloatProperty()
    rprop = jsonobject.ListProperty(float)
    xqc = jsonobject.ListProperty(float)
    zqc = jsonobject.ListProperty(float)
    opaz = jsonobject.IntegerProperty()
    blade_spacing = jsonobject.ListProperty(float)
    redge = jsonobject.ListProperty(float)
    rpos = jsonobject.ListProperty(float)


class Interface(jsonobject.JsonObject):
    """ Interface class represents a jsonobject that stores the data for each data interface between
     TAU and preCICE

    Attributes:
        name: name of the data interface
        mesh: name of the data interface mesh, must match mesh name in preCICE
        boundary_markers: list of lists of boundary markers, must match markers in TAU
        size: if boundary markers are not defined, the size of buffer is set by size value
        read_data: list of strings with the data types read from this interface
        write_data: list of strings with the data types written to this interface
    """

    name = jsonobject.StringProperty()
    mesh = jsonobject.StringProperty()
    size = jsonobject.IntegerProperty()
    read_data = jsonobject.ListProperty(str)
    write_data = jsonobject.ListProperty(str)
    blade = jsonobject.IntegerProperty()


class Simulation(jsonobject.JsonObject):
    """ Simulation class represents a jsonobject that stores the data for a helicopter simulation

    Attributes:
        results_dict (str): prefix of result file as string
        delta_table_prefix (str): prefix for delta table as string

        total_azimuths (int): total number of azimuths in simulation as int
        iteration (int): current coupled iteration number as int. used to set starting iteration
        if restarting
        blades (int): number of blades in simulation as int

        azimuths (list): list of azimuth positions as angles from starting position, length
        of total_azimuths. Used internally to pass information between datahandlers.
        data (dict): dict used by simulation to pass data between datahandlers
    """
    results_dict = jsonobject.StringProperty(required=True)
    delta_table_location = jsonobject.StringProperty(required=True)
    delta_table_prefix = jsonobject.StringProperty(required=True)

    iteration = jsonobject.IntegerProperty(default=0)
    blades = jsonobject.IntegerProperty(required=True)

    azimuths = jsonobject.ListProperty(float)

    blade_width = jsonobject.FloatProperty()
    blade_thickness = jsonobject.FloatProperty()
    heightwise_elements = jsonobject.IntegerProperty()
    lengthwise_elements = jsonobject.IntegerProperty()
    widthwise_elements = jsonobject.IntegerProperty()
    num_collocation_points = jsonobject.IntegerProperty()
    intermediate_mesh = jsonobject.ListProperty(float)
    intermediate_mesh_collocation_point_ids = jsonobject.ListProperty(int)

    interpolation_type = jsonobject.StringProperty()
    num_harmonics = jsonobject.IntegerProperty(default=1)


class Config(jsonobject.JsonObject):
    """ Config class represents a jsonobject that stores the data needed to configure the tau-python
     precice interface

    Attributes:
        debug (bool): boolean flag. debug values will be printed in log if set to true. default to
        false if not set

        logging_location (str): logging file folder location as str
        logging_name (str): name of logging file as str
        output_location (str): output file folder location as str
        output_prefix (str): prefix for output files as str

        precice (bool): boolean flag. will run in stand-alone (without preCICE if false). default to
        true if not set

        precice_config (str): relative or absolute path to precice configuration xml file as str

        interfaces (list): list of Interface class objects used in the simulation
        simulation (Simulation): Simulation class objects used to store simulation data and
        parameters
    """
    debug = jsonobject.BooleanProperty(default=False)

    logging_location = jsonobject.StringProperty(required=True)
    logging_name = jsonobject.StringProperty(required=True)

    output_location = jsonobject.StringProperty(required=True)
    output_prefix = jsonobject.StringProperty(required=True)

    plot_location = jsonobject.StringProperty()
    plot_prefix = jsonobject.StringProperty()

    job_file = jsonobject.StringProperty(required=True)
    job_info_file = jsonobject.StringProperty(required=True)

    overwrite_data = jsonobject.BooleanProperty()

    participant = jsonobject.StringProperty(required=True)
    partner = jsonobject.StringProperty()
    shared_folder = jsonobject.StringProperty()
    precice_config = jsonobject.StringProperty()

    interfaces = jsonobject.ListProperty(jsonobject.ObjectProperty(Interface))
    simulation = jsonobject.ObjectProperty(Simulation)
    rotor_info = jsonobject.ObjectProperty(RotorInfo)
