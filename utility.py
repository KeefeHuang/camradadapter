# 2017-07-19
"""Utility functions for CAMRAD/TAU coupling scripts.

Provides:
    set_axes_equal: set axes of a 3D plot to all have the same scale
    netcdf_dict: read data from netcdf file and save in dictionary
    rotation_matrix: get direction cosine matrix for a rotation about a base
        axis
    input_check: read and validate parameters from input file

"""

import logging
import sys
from os.path import join, isdir
from os import makedirs
import yaml
from camradadapter.config import Config


def read_config(config_file, config_class=Config):
    """Function that reads configuration file text and converts it to a dictionary

    Arguments:
        config_file (str): name of config file as str
        config_class (class): Config jsonobject used to parese input yaml file

    Returns: config file data stored as a dict
    """
    with open(config_file, "r") as file_handle:
        config_dict = yaml.load(file_handle, Loader=yaml.FullLoader)

    base_config = config_class(config_dict)

    return base_config


def initialize_logging(config):
    """Function that reads configuration file text and converts it to a dictionary

    Arguments:
        config (camrad_config.Config): config file data stored as dict
    """
    logger = logging.getLogger("camrad")
    logger.setLevel(logging.DEBUG)
    if not isdir(config.logging_location):
        create_necessary_folder(config.logging_location, logger)
    f_handler = logging.FileHandler(join(config.logging_location, config.logging_name))
    f_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    f_handler.setLevel(logging.DEBUG)
    f_handler.setFormatter(f_format)
    logger.addHandler(f_handler)

    c_handler = logging.StreamHandler()
    c_format = logging.Formatter('%(levelname)s: %(message)s')
    c_handler.setLevel(logging.INFO)
    c_handler.setFormatter(c_format)
    logger.addHandler(c_handler)

    if config.debug:
        debug_handler = logging.FileHandler("./debug.log")
        debug_format = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        debug_handler.setLevel(logging.DEBUG)
        debug_handler.setFormatter(debug_format)
        logger.addHandler(debug_handler)

    logger.info("Logger setup")


def create_necessary_folder(folder_name, logger):
    """ Creates a necessary folder. If folder does not exist or cannot be created, the program
    will exist with error signal

    Arguments:
        folder_name (str): key for folder in utility.Config object
        logger (logging.Logger): logger object to log result of folder creation
    """
    logger.info("Trying to make folder: {}".format(folder_name))
    try:
        makedirs(folder_name)
        logger.info("%s folder created", folder_name)
    #     Broad exception is used because exceptions are stored in log file
    except Exception:  # pylint: disable=broad-except
        # broad exception allowed since we are storing exception data
        logger.exception("Unable to create a %s folder", folder_name)
        sys.exit(1)
