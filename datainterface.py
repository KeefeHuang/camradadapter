"""
@package tauadapter
Python wrapper to control the execution of TAU coupled with preCICE
"""

import logging
from copy import deepcopy
import numpy as np
from mpi4py import MPI


class DataInterface(object):
    """ DataInterface class controls the communication between CAMRAD and precice\n

    Attributes:
        config (utility.Config): Utility.Config dictionary object created from input config file,
            parsed with Utility.config()
        precice_interface (precice.Interface): preCICE interface object. Allows read/write from/to
            preCICE
        data_readers (list): list of datahandler.DataHandler objects that handle data read from
        preCICE interface
        data_writers (list): list of datahandler.DataHandler objects that handle data sent to
            preCICE interface
        num_data_readers (int): number of data readers as int
        num_data_writers (int): number of data writers as int

    """

    def __init__(self, precice_interface, config):
        """ DataInterface constructor\n
        Sets precice_interface and config member variables

        Arguments:
            precice_interface (precice.Interface): preCICE interface object. Allows read/write to
                preCICE
            config (utility.Config): Utility.Config dictionary object created from input
                config file, parsed with Utility.config()
        """
        rank = MPI.COMM_WORLD.Get_rank()
        self.logger = logging.getLogger("tau" + str(rank) + ".data_interface")

        self.config = config
        self.simulation = config.simulation
        self.precice_interface = precice_interface
        self.rotor_info = config.rotor_info

        self.data_readers = []
        self.data_writers = []
        self.force_moment_readers = []

        self.num_data_readers = 0
        self.num_data_writers = 0

    def add_read_data_hander(self, data_reader):
        """ Adds DataReader object to self.data_readers

        Arguments:
            data_reader (datahandler.DataHandler): DataReader object

        Returns:
            index of added DataReader object in self.data_readers
        """
        self.data_readers.append(data_reader)
        self.num_data_readers += 1
        return self.num_data_readers - 1

    def add_write_data_hander(self, data_writer):
        """ Adds DataWriter object to self.data_readers

        Arguments:
            data_writer (datahandler.DataHandler): DataWriter object

        Returns:
            index of added DataReader object in self.data_readers
        """
        self.data_writers.append(data_writer)
        self.num_data_writers += 1
        return self.num_data_writers - 1

    def add_force_moment_readers(self, data_writer):
        """ Adds DataHandler object to self.force_moment_readers

        Arguments:
            data_writer (datahandler.DataHandler): DataHandler object

        Returns:
            index of added DataReader object in self.data_readers
        """
        self.force_moment_readers.append(data_writer)

    def read_all(self, iteration):
        """ Function loops through all existing data readers, communicates with preCICE interface
        and passes data to the DataReader objects for post-processing

        Arguments:
            iteration (int): the current coupling iteration
        """
        self.logger.info("Reading data from preCICE at iteration %s", iteration)
        if self.precice_interface.is_read_data_available():
            dimensions = self.precice_interface.get_dimensions()
            for data_reader in self.data_readers:
                if data_reader.is_scalar():
                    data = np.zeros(data_reader.length)
                    self.precice_interface.read_block_scalar_data(
                        data_reader.data_id,
                        data_reader.length,
                        data_reader.precice_vertex_ids,
                        data
                    )
                else:
                    data = np.zeros((dimensions * data_reader.length))
                    self.precice_interface.read_block_vector_data(
                        data_reader.data_id,
                        data_reader.length,
                        data_reader.precice_vertex_ids,
                        data
                    )
                data_reader.read(data, iteration)
        self.logger.info("Completed reading data to preCICE at iteration %s", iteration)

    def write_all(self, iteration):
        """ Function loops through all existing data writers, communicates with preCICE interface
        and passes data from the DataWriter objects to preCICE

        Arguments:
            iteration (int): the current coupling iteration
        """
        self.logger.info("Writing data to preCICE at iteration %s", iteration)
        for data_writer in self.data_writers:
            data = data_writer.write(iteration)

            if data_writer.is_scalar():
                self.precice_interface.write_block_scalar_data(
                    data_writer.data_id,
                    data_writer.length,
                    data_writer.precice_vertex_ids,
                    data
                )
            else:
                self.precice_interface.write_block_vector_data(
                    data_writer.data_id,
                    data_writer.length,
                    data_writer.precice_vertex_ids,
                    data
                )
        self.logger.info("Completed writing data to preCICE at iteration %s", iteration)

    def read_forces_moments(self):
        """ Reads forces moments data from fluid solver and arranges it depending on
        piece-wise or full

        Returns:
            tau_data (ndarray): Ndarray that stores both force and moment data from fluid
            solver.Ndarray of size (blades x collocation_points x azimuths x 6). When indexing
            the last value, the data is stored as (Fx, Fy, Fz, Mx, My, Mz)
            num_azimuths (int): number of azimuth positions in tau as int
            tau_azimuths (list): list of tau azimuth positions for the received force/moment
            data. list of length num_azimuths.
        """
        num_collocation_points = len(self.rotor_info.redge) - 1

        num_azimuths = self.simulation.total_azimuths
        tau_azimuths = np.array(self.simulation.azimuths)
        # Allocate memory for force and moment data
        moments = np.zeros((self.simulation.blades, self.simulation.num_collocation_points,
                            num_azimuths,
                            3))
        forces = np.zeros((self.simulation.blades, self.simulation.num_collocation_points,
                           num_azimuths,
                           3))
        # Create a data buffer to store force/moment data received from Fluid solver
        intermediate_grid = np.array(self.simulation.intermediate_mesh)
        intermediate_mesh_collocation_ids = np.array(
            self.simulation.intermediate_mesh_collocation_point_ids)
        temp_data_buffer = np.zeros(intermediate_grid.shape)
        # Read Force/Moment data
        for data_reader in self.force_moment_readers:
            for i, data_name in enumerate(data_reader.data_names):
                # Allocate memory to perform calculations to map intermediate mesh to 1D
                # collocation points
                data = np.zeros((num_collocation_points, 3))
                blade = int(data_name.split("_")[-2])
                azimuth = int(data_name.split("_")[-1])
                data_id = data_reader.data_id[i]

                self.precice_interface.read_block_vector_data(data_id, data_reader.length,
                                                              data_reader.precice_vertex_ids,
                                                              temp_data_buffer)
                for j in range(num_collocation_points):
                    new_data = temp_data_buffer.reshape((-1, 3))[
                        np.where(intermediate_mesh_collocation_ids == j)[0], :]
                    new_data = np.sum(new_data, axis=0)
                    data[j, :] = new_data
                if "Force" in data_name:
                    np.save("forces_" + str(azimuth), data)
                    forces[blade, :, azimuth, :] = deepcopy(data)
                if "Moment" in data_name:
                    np.save("moments_" + str(azimuth), data)
                    moments[blade, :, azimuth, :] = deepcopy(data)

        # Rearrange data so it aligns!
        # Numbering of blades goes in clockwise direction
        # For blade 0, azimuth position 0 aligns with angle == 0
        # For blade 1, azimuth position 0 aligns with angle == 1 * blade_angle, where blade angle
        # = 360 / num_blades. Thus, in order for the force/moment data from each blade to align,
        # the order of azimuth positions must be shifted. by blade # * blade_angle (to align with
        # blade 0)
        azimuths_per_blade = int(num_azimuths / self.simulation.blades)
        for blade in range(self.simulation.blades):
            force = forces[blade, :, :-1, :]
            force = np.roll(force, azimuths_per_blade * ((blade+1)%self.simulation.blades), 1)
            forces[blade, :, :-1, :] = force
            forces[blade, :, -1, :] = forces[blade, :, 0, :]
            moment = moments[blade, :, :-1, :]
            moment = np.roll(moment, azimuths_per_blade * ((blade+1)%self.simulation.blades), 1)
            moments[blade, :, :-1, :] = moment
            moments[blade, :, -1, :] = moments[blade, :, 0, :]

        forces = np.average(forces, axis=0)
        moments = np.average(moments, axis=0)
        tau_data = np.concatenate((forces, moments), axis=2)

        return tau_data, num_azimuths, tau_azimuths

    def read(self, iteration, index):
        """ Function loops through all existing data readers, communicates with preCICE interface
        and passes data to the DataReader objects for post-processing

        Arguments:
            iteration (int): the current coupling iteration
            index (int): index of data reader as int
        """
        self.logger.info("Reading data from preCICE data_reader %s at iteration %s",
                         self.data_readers[index].mesh_id, iteration)
        read_data = []
        if self.precice_interface.is_read_data_available():
            dimensions = self.precice_interface.get_dimensions()
            data_reader = self.data_readers[index]

            for _, data_id in enumerate(data_reader.data_id):
                if data_reader.is_scalar():
                    data = np.zeros(data_reader.length)
                    self.precice_interface.read_block_scalar_data(
                        data_id,
                        data_reader.length,
                        data_reader.precice_vertex_ids,
                        data
                    )
                else:
                    data = np.zeros((dimensions * data_reader.length))
                    self.precice_interface.read_block_vector_data(
                        data_id,
                        data_reader.length,
                        data_reader.precice_vertex_ids,
                        data
                    )
                data_reader.read(data, iteration)
                read_data.append(data)
        self.logger.info("Completed reading data from preCICE data_reader %s at iteration %s",
                         self.data_readers[index].mesh_id, iteration)
        return read_data

    def write(self, iteration, index):
        """ Function loops through all existing data writers, communicates with preCICE interface
        and passes data from the DataWriter objects to preCICE

        Arguments:
            iteration (int): the current coupling iteration
            index (int): index of data reader as int
        """
        self.logger.info("Writing data from preCICE data_writer %s at iteration %s",
                         self.data_readers[index].mesh_id, iteration)
        data_writer = self.data_writers[index]
        data = data_writer.write(iteration)
        for i, data_id in enumerate(data_writer.data_id):

            if data_writer.is_scalar():
                self.precice_interface.write_block_scalar_data(
                    data_id,
                    data_writer.length,
                    data_writer.precice_vertex_ids,
                    data[i]
                )
            else:
                self.precice_interface.write_block_vector_data(
                    data_id,
                    data_writer.length,
                    data_writer.precice_vertex_ids,
                    data[i]
                )
        self.logger.info("Completed writing data to preCICE at iteration %s", iteration)
