"""
@package tauadapter
Python wrapper to control the execution of TAU coupled with preCICE
"""
# pylint: disable=duplicate-code
# pylint: disable=import-error
import logging
from os.path import join, isfile, isdir
import os
import pickle
import sys
import time
import re

import numpy as np
import precice  # pylint: disable=import-error

import camradadapter.table as table

import camradadapter.camradfunctions as camrad
import camradadapter.utility as utility

from camradadapter.vectorhandler import VectorHandler
from camradadapter.azimuthshandler import AzimuthsHandler
from camradadapter.collocationshandler import CollocationsHandler
from camradadapter.motionshandler import MotionsHandler
from camradadapter.datainterface import DataInterface
from camradadapter.variables import Variables
from camradadapter.config import Config
from camradadapter.datahandler import DataHandler

from camradadapter.config import RotorInfo

DATA_WRITERS = {"Collocations": CollocationsHandler, "Motions": MotionsHandler}
DATA_READERS = {"Azimuths": AzimuthsHandler}


# pylint: disable=duplicate-code
class Adapter(object):
    """ CamradSolver class. Runs coupled Camrad simulation using preCICE

    Attributes:
        config (camrad_config.Config): Config jsonobject used to store configuration information
        simulation (camrad_config.Simulation): Simulation jsonobject used to stored simulation data
        rotor_info (dict): dict object that stores CAMRAD simulation data
        precice_interface (precice.Interface): preCICE interface object. used to control coupled
            simulation.
        data_interface (datainterface.DataInterface): DataInterface object. used to pass
            information between simulation and preCICE.
        logger (logging.Logger): Logger object
    """
    CONFIG_CLASS = None

    # pylint: disable=no-member
    def __init__(self, config_file):
        """ CamradSolver constructor. Reads config file and initializes logging. Initializes
        preCICE environment and DataInterface object

        Arguments:
            config_file (str): name of CamradSolver configuration file as str"""
        if self.CONFIG_CLASS is not None:
            self.config = utility.read_config(config_file, self.CONFIG_CLASS)
        else:
            self.config = utility.read_config(config_file)
        self.simulation = self.config.simulation
        self.rotor_info = self.config.rotor_info = RotorInfo()

        utility.initialize_logging(self.config)

        self.logger = logging.getLogger("camrad")

        # Write log file header
        self.logger.info('Coupled CAMRAD II/TAU simulation')
        self.logger.info('Simulation start time: %s', time.strftime("%Y-%m-%d %H:%M:%S"))

        self.solver_name = "CAMRAD"

        # Create rotor_info dictionary from config file
        camrad.read_camrad_data(self.config.job_info_file, self.config)

        try:
            precice_configuration_file = str(self.config.precice_config)
        except IOError:
            self.logger.error("Error: Precice config file '%s' not found.",
                              self.config.precice_config)
            sys.exit(1)

        # Initialize precice interface
        try:
            self.precice_interface = precice.Interface(self.solver_name, 0, 1)
            self.precice_interface.configure(precice_configuration_file)
            self.logger.info("Configuring preCICE")
        except TypeError:
            self.precice_interface = precice.Interface(self.solver_name,
                                                       precice_configuration_file, 0, 1)
        # Initialize data interface
        self.data_interface = DataInterface(self.precice_interface, self.config)

        # Initialize other empty member variables
        self.dimensions = 0

        if self.simulation.interpolation_type == "piece-wise":
            self.simulation.blades = 1

        self.exchange_variables()

    def exchange_variables(self):
        """ Method that determined what variables need to be passed to other solver and fills
        write_dict. Instantiates Variables object and passes data via written yaml file.
        """
        write_dict = {}

        redge = [x for x in self.rotor_info.redge]
        rpos = [x for x in self.rotor_info.rpos]
        collocation_size = 4 * len(redge) - 3
        num_collocation_points = len(redge) - 1
        write_dict['interfaces'] = [{'read_data': ['Collocations0'], 'size': collocation_size}]
        write_dict['simulation'] = {'num_collocation_points': num_collocation_points,
                                    'redge': redge, 'rpos': rpos}

        omega = self.rotor_info.omega
        num_harmonics = self.simulation.num_harmonics
        motion_size = 4 * (2 * self.simulation.num_harmonics + 1) * (len(rpos))

        write_dict['interfaces'] += [{'read_data': ['Motions0'], 'size': motion_size}]
        write_dict['simulation'].update({'omega': omega, 'num_harmonics': num_harmonics})

        variables = Variables(self.config, write_dict)
        variables.exchange_variables()
        for interface in self.config.interfaces:
            if interface.mesh == "CAMRAD_Collocations":
                interface.size = collocation_size
            if interface.mesh == "CAMRAD_Motions":
                interface.size = motion_size

    def initialize(self):
        """ Initializes various components of CamradSolver. Configures preCICE environment, sets up
        folder structure for CAMRAD simulation.
        """
        self.dimensions = self.precice_interface.get_dimensions()

        self.logger.info("preCICE configured...")

        self.logger.info("Checking if required directories are present")
        # Setup necessary and optional folders if possible.
        necessary_folders = [self.config.output_location,
                             self.config.simulation.delta_table_location,
                             self.config.plot_location]

        for folder in necessary_folders:
            if not isdir(folder):
                utility.create_necessary_folder(folder, self.logger)
        self.setup_data_handlers()

    # pylint: disable=no-member
    def setup_data_handlers(self):
        """ Sets up DataHandler objects for CAMRAD simulation. Creates an intermediate grid that
        acts as intermediary between 3D fluid solver discretisation and 1D CAMRAD discretisation"""
        # Check input file and update logging dictionary
        self.logger.info("Setup CAMRAD Simulation")

        self.logger.info("Setting up meshing in preCICE")

        # Create intermediate mesh based on collocation point data and config values
        camrad.create_intermediate_mesh(self.rotor_info, self.config)

        mesh_size = np.array(self.simulation.intermediate_mesh).shape[0] // 3
        # Create data readers for the Force/Moment data

        data_type_pattern = re.compile(r"\w[^_0-9]+")

        for interface in self.config.interfaces:
            if interface.size is None:
                mesh_id = self.precice_interface.get_mesh_id(str(interface.mesh))
                data_ids = []
                for data_reader in interface.read_data:
                    data_ids.append(self.precice_interface.get_data_id(str(data_reader),
                                                                       mesh_id))
                precice_ids = np.zeros(mesh_size)
                size = precice_ids.shape[0]

                self.precice_interface.set_mesh_vertices(mesh_id, size,
                                                         np.array(self.simulation.intermediate_mesh)
                                                         , precice_ids)
                self.data_interface.add_force_moment_readers(
                    VectorHandler(mesh_id, data_ids, precice_ids, interface.read_data)
                )
            else:
                buffer_size = interface.size
                data_buffer = np.array([[] + [0, 0, i] for i in range(buffer_size)]).flatten()
                vertex_ids = np.zeros(buffer_size)
                mesh_name = str(interface.mesh)
                mesh_id = self.precice_interface.get_mesh_id(mesh_name)
                self.precice_interface.set_mesh_vertices(mesh_id, buffer_size, data_buffer,
                                                         vertex_ids)
                if interface.read_data:
                    for read_data_name in interface.read_data:
                        read_data_id = self.precice_interface.get_data_id(str(read_data_name),
                                                                          mesh_id)
                        data_type = re.search(data_type_pattern, str(read_data_name)).group(0)
                        self.data_interface.add_read_data_hander(
                            DATA_READERS[data_type](mesh_id, str(read_data_id), vertex_ids,
                                                    self.config))
                if interface.write_data:
                    for write_data_name in interface.write_data:
                        write_data_id = self.precice_interface.get_data_id(str(write_data_name),
                                                                           mesh_id)
                        data_type = re.search(data_type_pattern, str(write_data_name)).group(0)
                        self.data_interface.add_write_data_hander(
                            DATA_WRITERS[data_type](mesh_id, str(write_data_id), vertex_ids,
                                                    self.config))

    def execute(self):
        """ Execution loop of coupled CAMRAD simulation. Runs CAMRAD executable with jobfile as
        input for simulation step. Calls methods to send/receive data from fluid solver"""
        # Get current coupling count from config file
        count = self.simulation.iteration

        # Set the preCICE timestep as 1
        time_step = 1

        # Create delta table with zeros for first iteration
        if count <= 1:
            zero_table = str(join(self.simulation.delta_table_location,
                                  self.simulation.delta_table_prefix)) + "0"
            table.create_zeros_table(zero_table)

        self.logger.info("CAMRAD Simulation configured")
        _ = self.precice_interface.initialize()

        if self.precice_interface.is_action_required(precice.action_write_initial_data()):
            # Package collocation data from camrad inputs and pass this as initial data
            self.precice_interface.fulfilled_action(precice.action_write_initial_data())

        self.precice_interface.initialize_data()

        while self.precice_interface.is_coupling_ongoing():

            # When an implicit coupling scheme is used, checkpointing is required
            if self.precice_interface.is_action_required(
                    precice.action_write_iteration_checkpoint()):
                self.precice_interface.fulfilled_action(precice.action_write_iteration_checkpoint())

            camrad_out_file = self.update_jobfile(count)

            self.data_interface.write_all(count)
            # When an implicit coupling scheme is used, checkpointing is required
            if self.precice_interface.is_action_required(
                    precice.action_read_iteration_checkpoint()):
                self.precice_interface.fulfilled_action(precice.action_read_iteration_checkpoint())
            else:
                self.precice_interface.advance(time_step)
                count += 1

            if self.precice_interface.is_read_data_available():
                self.data_interface.read_all(count)

                tau_data, _, tau_azimuths = self.data_interface.read_forces_moments()

                camrad_azimuths, camrad_data, radial_stations, tau_data = self.create_delta_table(
                    camrad_out_file, count, tau_azimuths, tau_data)

                controls = camrad.get_control_inputs(camrad_out_file)

                # Save results to dictionary for pickling
                self.write_results_file(camrad_azimuths, camrad_data, controls, count,
                                        radial_stations, tau_azimuths, tau_data)

        self.precice_interface.finalize()

    def create_delta_table(self, camrad_out_file, count, tau_azimuths, tau_data):
        """ Creates delta table of aerodynamic loads for subsequent CAMRAD simulation.
        Interpolates fluid solver data to match CAMRAD simulation input discretisation. Writes
        delta table

        Arguments:
            camrad_out_file (str): name of CAMRAD output file as string
            count (int): coupling iteration as int
            tau_azimuths (list): list of TAU azimuths, temporal discretisation of fluid solver data
            tau_data (ndarray): ndarray containing force/moment data from fluid solver. Array of
                size (collocation_points x azimuths x 6). 3D values for Forces/Moments
                ordered as (Fx, Fy, Fz, Mx, My, Mz).
        """

        radial_stations, camrad_azimuths, camrad_data = camrad.camrad_blade_forces_moments(
            camrad_out_file)
        collocation_points = camrad.package_collocation_data(self.rotor_info)
        tau_data = table.interpolate_data(collocation_points, tau_azimuths, tau_data,
                                          camrad_azimuths)
        if count <= 1:
            prev_table = None
        else:
            prev_table = join(self.simulation.delta_table_location,
                              self.simulation.delta_table_prefix) + str(count - 1)
        # Name of new table
        table_name = join(self.simulation.delta_table_location,
                          self.simulation.delta_table_prefix) + str(count)
        table.create_delta_table(tau_azimuths, tau_data, radial_stations,
                                 camrad_data, table_name, 0.9, prev_table=prev_table)
        return camrad_azimuths, camrad_data, radial_stations, tau_data

    def update_jobfile(self, count):
        """ Updates CAMRAD jobfile to change output file name, output plot name, delta table name
        before each coupling.

        Arguments:
            count (int): number of coupling iterations as int
        """
        # Change output location in CAMRAD job file
        with open(self.config.job_file, 'r') as job_file:
            file_data = list(job_file)
        # Find line number for output name and save text
        name_index, _ = [(i, s) for i, s in enumerate(file_data) if 'set name='
                         in s][0]
        # Find line number for plot file name and save text
        plot_index, _ = [(i, s) for i, s in enumerate(file_data) if 'setenv P'
                                                                    'LOTFILE' in s][0]
        # Find line number for OPINIT
        input_index, _ = [(i, s) for i, s in enumerate(file_data) if 'OPITJR'
                          in s][0]
        # Find line number for table name ('EXLOG') and save text
        table_index, _ = [(i, s) for i, s in enumerate(file_data) if 'EXLOG'
                          in s][0]

        if count > 1:
            input_line = "OPITJR=1,\n"
        else:
            input_line = "OPITJR=0,\n"
        file_data[input_index] = input_line
        # Redirect output to new directory and rewrite file with new line
        # Outfolder is hardcoded to a specific location
        output_name = join(self.config.output_location, self.config.output_prefix) + str(count)
        camrad_output_line = "set name='{}'".format(output_name) + "\n"
        file_data[name_index] = camrad_output_line
        # modify input data each time -> This links to the table output created by TAU solver
        # Save new plot file name
        plot_name = join(self.config.plot_location, self.config.plot_prefix) + str(count)
        plot_file_line = 'setenv PLOTFILE $plots/{}.plot\n'.format(plot_name)
        file_data[plot_index] = plot_file_line
        # Change table name to one in input file
        current_table = join(self.simulation.delta_table_location, self.simulation
                             .delta_table_prefix) + str(count)
        camrad_table_line = ("     EXLOG= '" + current_table + "',\n")
        file_data[table_index] = camrad_table_line
        # Rewrite job file with new data
        with open(self.config.job_file + str(count), 'w') as out_file:
            for item in file_data:
                out_file.write(item)
        # Get name of CAMRAD output file
        camrad_out_file = camrad_output_line.split('=')[-1][1:-2]
        # Perform error checking in CAMRAD
        if isfile(camrad_out_file) and not self.config.overwrite_data:
            self.check_camrad_output(camrad_out_file)
        # Run CAMRAD if data is missing or data should be overwritten
        if not isfile(camrad_out_file) or self.config.overwrite_data:
            self.logger.info("Running CAMRAD job '%s' and writing output to '%s'",
                             self.config.job_file, camrad_out_file)
            os.system('tcsh {}'.format(self.config.job_file + str(count)))
            os.system('cp TRIMSOLUTIONOUT1 TRIMSOLUTIONIN1')
            os.system('mv TRIMSOLUTIONOUT1 TRIMSOLUTION' + str(count))
        else:
            self.logger.info("CAMRAD file '%s' already present", camrad_out_file)
        return camrad_out_file

    def write_results_file(self, camrad_azimuths, camrad_data, controls, count, r_stations,
                           tau_azimuths, tau_data):
        """ Write results file for post-processing. Contains forces/moments from fluid solver and
        CAMRAD, CAMRAD control output, radial stations in CAMRAD, temporal discretisation of
        revolution.

        Arguments:
            camrad_azimuths (list): list of CAMRAD azimuths, temporal discretisation of fluid
            solver data
            camrad_data (ndarray): ndarray containing force/moment data from fluid solver. Array of
                size (collocation_points x azimuths x 6). 3D values for Forces/Moments
            tau_azimuths (list): list of TAU azimuths, temporal discretisation of fluid solver data
            tau_data (ndarray): ndarray containing force/moment data from fluid solver. Array of
                size (collocation_points x azimuths x 6). 3D values for Forces/Moments
                ordered as (Fx, Fy, Fz, Mx, My, Mz).
            controls (list): list of controls
            count (int): number of coupling iterations as int
            r_stations (list): list of radial stations
        """
        results_dict = {'rStations': r_stations, 'camradAngles': camrad_azimuths,
                        'camradData': camrad_data, 'tauData': tau_data,
                        'tauAngles': tau_azimuths,
                        'controls': controls}
        # Increment results file name
        output_name = str(self.simulation.results_dict) + str(count)
        self.logger.info("Saving data to %s", output_name)
        with open(output_name, "wb") as results_filename:
            pickle.dump(results_dict, results_filename, 2)

    def check_camrad_output(self, camrad_out_file):
        """ Method to check CAMRAD output for errors. Will force CAMRAD to run again if errors
        are non-zero in output file

        Arguments:
            camrad_out_file (str): name of CAMRAD output file as str
        """
        error_pattern = re.compile(
            r"\*+ ERROR \*+ INPUT; ERROR COUNT NOT ZERO AT END OF CASE INPUT")
        error_capture_pattern = re.compile(
            r"(\*+ ERROR \*+ [0-9A-z/,=;() ]*\n(\s\s+[0-9A-z/.,=;() ]*\n)*)")
        success_pattern = re.compile(r"ANALYSIS EXIT \(END OF PROGRAM\)")
        with open(camrad_out_file, "r") as camrad_file:
            camrad_data = camrad_file.read()
        try:
            re.search(error_pattern, camrad_data).group(0)
            error_list = re.findall(error_capture_pattern, camrad_data)
            for error in error_list:
                self.logger.info("Error found in CAMRAD output file:\n%s", error[0])
            self.logger.info("Errors found in CAMRAD output data. Re-running simulation")
            self.config.overwrite_data = True
        except AttributeError:
            try:
                re.search(success_pattern, camrad_data).group(0)
                self.logger.info("Previous CAMRAD output terminated successfully, proceeding")
            except AttributeError:
                self.logger.info("Previous CAMRAD output did not terminate properly. Re-running "
                                 "simulation")
                self.config.overwrite_data = True

    @staticmethod
    def update_config(config_class):
        """ Static method used to update Config class in CAMRAD Adapter class
        Arguments:
            config_class (class): subclass of Config class used to read yaml files
        """
        if issubclass(config_class, Config):
            Adapter.CONFIG_CLASS = config_class
        else:
            raise TypeError("{} is not a subclass of Config".format(config_class))

    def add_data_handlers(self, class_dict, data_handler_type):
        """ Method to add new datahandler types to coupled simulation

        Arguments:
            class_dict (dict): dict containing key-value pair, where key is the name of the new
                datahandler and value is the datahandler class type.
            data_handler_type (str): updates DATA_READERS or DATA_WRITERS dict depending on input
        """
        for class_type in class_dict.values():
            if not issubclass(class_type, DataHandler):
                self.logger.error("%s is not a subclass of DataHandler", str(class_type))
                sys.exit()
        if data_handler_type.lower() == "read":
            DATA_READERS.update(class_dict)
        elif data_handler_type.lower() == "write":
            DATA_WRITERS.update(class_dict)
        else:
            self.logger.error("DataHandler type %s is not recognised, please input 'write' or "
                              "'read'", data_handler_type)
            sys.exit()

    def add_data_handler(self, data_handler_name, data_handler_class, data_handler_type):
        """  Method to add a single new datahandler type to coupled simulation

        Arguments:
            data_handler_name (str): name of datahandler in config file
            data_handler_class (class): class of new data handler
            data_handler_type (str): updates DATA_READERS or DATA_WRITERS dict depending on input
        """
        if not issubclass(data_handler_class, DataHandler):
            self.logger.error("%s is not a subclass of DataHandler", str(data_handler_class))
            sys.exit()

        class_dict = {data_handler_name: data_handler_class}

        if data_handler_type.lower() == "read":
            DATA_READERS.update(class_dict)
        elif data_handler_type.lower() == "write":
            DATA_WRITERS.update(class_dict)
        else:
            self.logger.error("DataHandler type %s is not recognised, please input 'write' or "
                              "'read'", data_handler_type)
            sys.exit()
