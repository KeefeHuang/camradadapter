"""
@package tauadapter
Python wrapper to control the execution of TAU coupled with preCICE
"""

import logging
from os.path import join
import math
import re

import numpy as np

from camradadapter.datahandler import DataHandler


class MotionsHandler(DataHandler):
    """ MotionsHandler class. Writes motion data from CAMRAD solver

    Attributes:
        precice_vertex_ids (list): list of unique integer ids identifying the specific vertices in
        the precice interface
        precice_vertex_ids (list): list of precice vertex_ids
    """

    # pylint: disable=duplicate-code
    def __init__(self, mesh_id, data_id, precice_vertex_ids, config):
        """ Constructor for ForcesHandler class.

        Arguments:
            mesh_id (int): unique integer id identifying the specific mesh in precice interface
            data_id (list): unique integer id identifying the specific data type in precice
                interface
            precice_vertex_ids (ndarray): list of unique integer ids identifying the specific
                vertices in the precice interface
            config (camard_utility.Config): camrad_utility.Config object storing simulation
                parameters
        """
        super(MotionsHandler, self).__init__(mesh_id, data_id, precice_vertex_ids)

        self.config = config
        self.simulation = config.simulation
        self.rotor_info = config.rotor_info

        self.logger = logging.getLogger("camrad.motionshandler")
        self.logger.info("Setting up motionshandler")

    def write(self, iteration):
        """ Implements datahandlers.DataHandler.write() abstract function.

        Arguments:
            iteration (int): current coupled iteration as int
        """
        self.logger.info("Writing motion data")
        output_name = join(self.config.output_location, self.config.output_prefix) + str(iteration)
        coeffs = self.get_blade_pos_data(4, output_name)

        return coeffs

    def read(self, data, iteration):
        """ Reads motion data from preCICE. Read is not implemented for motion data in CAMRAD

        Arguments:
            data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            iteration (int): current coupled iteration as int
        """
        self.logger.error("Read is not implemented for motion for this module\n")
        raise NotImplementedError

    # pylint: disable=duplicate-code
    def is_scalar(self):
        """ Function returns true if data handled is scalar. Motion data is scalar.
        Returns:
             Returns false as this is a vector data type
        """
        return True

    def get_blade_pos_data(self, blade, data_file):
        """Collect and return sensor position data from output file in arrays.
        Args:
            blade (int): blade number being tracked
            data_file (string): output file name (including path and extension)

        Returns:
            flap (array): flap data (len: num_harmonics*2 + 1) [deg]
            lag (array): lag data (len: num_harmonics*2 + 1) [deg]
            pitch (array): pitch data (len: num_harmonics*2 + 1) [deg]
            axial (array): axial data (len: num_harmonics*2 + 1) [deg]

        Notes:
        Data is formatted as [mean, cos1, sin1, cos2, sin2, ...]. This function
        works for any number of harmonics; however, the other scripts have only
        been tested for one. Check output if using more than one.
        """
        rotor_info = self.rotor_info
        num_harmonics = self.simulation.num_harmonics
        with open(data_file, 'r') as output_data:
            data = output_data.read()

        harmonic_pattern = re.compile(
            r"((?:\s*MEAN|\s*COSINE\s*[0-9]+|\s+SINE\s*[0-9]+)\s*([0-9.\-E]+)\s*([0-9.\-E]+)\s*"
            r"([0-9.\-E]+)\s*([0-9.\-E]+))")
        radial_positions = np.array(rotor_info.rpos)
        radial_positions = np.append(radial_positions, [1.0])
        # harmonics_data = [2 * num_harmonics + 1]
        harmonics_data = []

        for radial_position in radial_positions:
            self.logger.debug("Storing data for RPOS: %f", radial_position)
            # locate region in camrad output file that matches this radial position
            position_pattern = re.compile(
                r"BLADE {} POS\s*{:.4f}R".format(blade, radial_position))

            start_position = re.search(position_pattern, data).start(0)
            match_objects = re.finditer(harmonic_pattern, data[start_position:])
            flap = []
            lag = []
            pitch = []
            axial = []
            next(match_objects)
            for _ in range(num_harmonics * 2 + 1):
                match_data = next(match_objects)
                flap.append(float(match_data.group(2)))
                lag.append(float(match_data.group(3)))
                pitch.append(float(match_data.group(4)))
                axial.append(float(match_data.group(5)))

            flap = [math.degrees(math.atan(x / radial_position)) for x in flap]
            lag = [math.degrees(math.atan(x / radial_position)) for x in lag]
            axial = [math.degrees(math.atan(x / radial_position)) for x in axial]

            harmonics_data += flap + lag + pitch + axial

        return harmonics_data
