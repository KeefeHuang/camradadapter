"""
@package tauadapter
Python wrapper to control the execution of CAMRAD coupled with preCICE
"""

import logging
from camradadapter.datahandler import DataHandler


class AzimuthsHandler(DataHandler):
    """ AzimuthsHandler class. Reads azimuth data from preCICE

    Attributes:
        precice_vertex_ids (list): list of unique integer ids identifying the specific vertices in
        the precice interface
        precice_vertex_ids (list): list of precice vertex_ids
    """

    def __init__(self, mesh_id, data_id, precice_vertex_ids, config):
        """ Constructor for AzimuthsHandler class.

        Arguments:
            mesh_id (int): unique integer id identifying the specific mesh in precice interface
            data_id (list): unique integer id identifying the specific data type in precice
                interface
            precice_vertex_ids (ndarray): list of unique integer ids identifying the specific
                vertices in the precice interface
            config (camard_utility.Config): camrad_utility.Config object storing simulation
                parameters
        """
        super(AzimuthsHandler, self).__init__(mesh_id, data_id, precice_vertex_ids)

        self.config = config
        self.simulation = config.simulation

        self.logger = logging.getLogger("camrad.azimuthshandler")
        self.logger.info("Setting up azimuthshandler")

    def write(self, iteration):
        """ Writes azimuths data from preCICE. Write is not implemented for collocation
        points in CAMRAD

        Arguments:
            data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            iteration (int): current coupled iteration as int
        """
        self.logger.error("Write is not implemented for this module\n")
        raise NotImplementedError

    def read(self, data, iteration):
        """ Reads azimuths point data from preCICE.

        Arguments:
            data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            iteration (int): current coupled iteration as int
        """
        self.simulation.azimuths = list(data)

    def is_scalar(self):
        """ Function returns true if data handled is scalar. Azimuth data is scalar.

        Returns:
             Returns false as this is a vector data type
        """
        return True
