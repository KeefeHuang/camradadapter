# 2017-05-18
"""Functions to create tables in the format readable by CAMRAD.

Provides:
    create_table: generate generic table file for CAMRAD
    create_delta_table: create incremental load table based on forces/moments
        in TAU and CAMRAD
    create_zeros_table: create incremental load table for CAMRAD with all zeros
    read_table: read incremental load table and store data in numpy arrays

"""

import math
import logging
import time
import sys
from copy import deepcopy

import numpy as np
import scipy as sp
from scipy.interpolate import splrep, splev


def create_table(fname, alpha, beta, coeff, label_g):
    """Generate generic table file for CAMRAD II.

    Write a CAMRAD II table file (format=2D, type=standard) using the format in figure 26-2 in V6
    p.278. Each value is give 12 characters of space (allows for 7 significant figures using
    scientific notation). Max line length is 132 chars, so each line is wrapped after the 8th
    element (9 including alpha value). The size of the independent variables (alpha and beta) must
    be the same for each dependentvariable.

    Arguments:
        fname (string): name (including path/ext) of table file to be written
        alpha (ndarray): alpha values for vertical axis (shape: NA x NG)
        beta (ndarray): beta values for horizontal axis (shape: NB x NG)
        coeff (array): coeff (or load) values (shape: NA x NB x NG)
        label_g (list): codes (see V6 p.236) for coeff/load types (length: NG)
        log (string): name of parent logger (default: '')

    Parameters:
        title (string): title of table
        ident (string): identification code of file (default is current time)
        labelA (string): name of alpha values
        labelB (string): name of beta values

    """

    logger = logging.getLogger("camrad")

    title = "INCREMENT TEST"
    ident = time.strftime("%Y-%m-%d %H:%M:%S")
    label_a = "SPAN"
    label_b = "AZIMUTH"

    # Calculate size of each variable
    na_values = []
    nb_values = []
    ng_values = np.shape(alpha)[1]
    for i in range(ng_values):
        na_values.append(np.shape(alpha[:, i])[0])
        nb_values.append(np.shape(beta[:, i])[0])

    try:
        logger.info("Writing table %s", fname)
        with open(fname, 'w') as table_file:
            # Write file header
            table_file.write("{}\n{}\n".format(title, ident))
            table_file.write("{} {} {}\n".format(label_a, label_b, ng_values))

            # Write data for each coefficient
            for i in range(ng_values):
                table_file.write("{} {} {}".format(na_values[i], nb_values[i], label_g[i]))
                # Write beta column headers
                for j in range(nb_values[i]):
                    if j % 8 == 0:
                        table_file.write("\n              ")
                    table_file.write("{:<13.2f} ".format(beta[j, i]))
                table_file.write("\n")
                # Write alpha row headers followed by coefficient data
                for k in range(na_values[i]):
                    table_file.write("{:<13}".format(alpha[k, i]))
                    for j in range(nb_values[i]):
                        if j % 8 == 0 and j != 0:
                            table_file.write("\n             ")
                        table_file.write("{:13.6E} ".format(coeff[k, j, i]))
                    table_file.write("\n")
            table_file.write("END OF FILE")

    except IOError:
        logger.error("Error: invalid table file path '%s'.", fname)
        sys.exit(1)


def create_delta_table(tau_azimuth, tau_data, r_stations, camrad_data, table_name, relax,
                       prev_table=None):
    """Create incremental load table based on forces/moments in TAU and CAMRAD.

    Write a CAMRAD II table file (format=2D, type=standard) using the format in
    figure 26-2 in V6 p.278 with the difference between the forces/moments
    calculated in CAMRAD and in TAU. Azimuthal spacing in TAU and CAMRAD do not
    have to match, but TAU should have the finer spacing. The previous delta
    table must be in the same order and use the same radial and azimuthal
    spacing as the new data.

    Args:
        tau_azimuth (ndarray): azimuth angles from TAU data (length: n_azimuth_TAU)
        tau_data (ndarray): force/moment data from TAU data (size: n_panel x
            n_azimuth_TAU x 6)
        r_stations (ndarray): radial stations for sensor data (size: n_panel x 6)
        camrad_data (ndarray): force/moment data from TAU data (size: n_panel x
            n_azimuth_TAU x 6)
        table_name (string): name of final table to be created (including path
            and extension)
        relax (float): delta table data is multiplied by this factor
        prev_table (string): file name (including path and extension) of table
            file with previous delta values (default: '')
        log (string): name of parent logger (default: '')

    Parameters:
        labels (list): list of codes (see V6 p.236) for coeff/load types in
            table

    """

    logger = logging.getLogger("camrad")

    logger.info('Finding difference in TAU and CAMRAD data')

    # Labels for table entries (see V6 p.236)
    labels = ["FQCX", "FQCY", "FQCZ", "MQCX", "MQCY", "MQCZ"]

    # Calculate difference in data and make table of results
    delta_data = np.subtract(tau_data, camrad_data, out=None) #pylint: disable=assignment-from-no-return

    # Multiply by relaxation factor
    delta_data *= relax

    # Add previous delta data (must be same size and same order as new delta)
    if prev_table is not None:
        _, _, prev_c, prev_g = read_table(prev_table)
        if prev_g != labels:
            logger.error("Error: previous delta table is in the wrong order.")
            sys.exit(1)
        if np.shape(prev_c) != np.shape(delta_data):
            logger.error("Error: previous delta table data does not have the c"
                         "orrect size.")
            sys.exit(1)
        delta_data += prev_c

    # Replace delta data with 0.1 for testing (CAMRAD crashes if delta values
    # are too large)
    # deltaData = 0.1*np.ones(np.shape(deltaData))##############################

    # Repeat values NG times so dimensions match
    azimuth_angles = []
    for _ in range(len(labels)):
        azimuth_angles.append(tau_azimuth)
    azimuth_angles = np.transpose(np.array(azimuth_angles))

    create_table(table_name, r_stations, azimuth_angles, delta_data, labels)


def create_zeros_table(table_name):
    """Create incremental load table for CAMRAD with all zeros.

    Write a CAMRAD II table file (format=2D, type=standard) using the format in
    figure 26-2 in V6 p.278 with all sensor increments set to zero (effectively
    a placeholder table).

    Args:
        table_name (string): name of table to be created (including path and
            extension)
        log (string): name of parent logger (default: '')

    """

    logger = logging.getLogger("camrad")

    # Labels for table entries (see V6 p.236)
    label_g = ["CL", "CD"]

    a_range = np.array([[0, 0], [1, 1]])
    b_range = np.array([[0, 0], [360, 360]])

    c_range = np.zeros((2, 2, 2))

    logger.debug('Creating zeros table as a placeholder')
    create_table(table_name, a_range, b_range, c_range, label_g)


def read_table(table_file): #pylint: disable=too-many-statements
    """Read incremental load table and store data in numpy arrays.

    Read a CAMRAD II table file (format=2D, type=standard) written using the
    format in figure 26-2 in V6 p.278 and save data in numpy arrays.

    Args:
        table_file (string): name of table to be created (including path and
            extension)
        log (string): name of parent logger (default: '')

    Returns:
        alpha (array): alpha values for vertical axis (shape: NA x NG)
        beta (array): beta values for horizontal axis (shape: NB x NG)
        coeff (array): coeff (or load) values (shape: NA x NB x NG)
        labelG (list): codes (see V6 p.236) for coeff/load types (length: NG)

    Notes:
    Table files should not be too large since the entire file is read into
    memory before parsing. NA and NB (size of independent variables, e.g. SPAN
    and AZIMUTH) must be the same for each dependent variable)

    """

    logger = logging.getLogger("camrad")

    alpha = []
    beta = []
    coeff = []
    label_g = []

    try:
        with open(table_file, 'r') as table:
            # Skip title and table ID lines
            table.next()
            table.next()
            # Get list of all table entries
            table_data = [word for line in table for word in line.split()]
    except IOError:
        logger.error("Error: table file '%s' not found:", table_file)
        sys.exit(1)
    # Save data labels and number of blocks
    label_a = table_data[0]
    label_b = table_data[1]
    # Get number of blocks (dependent variables)
    try:
        num_blocks = int(table_data[2])
    except ValueError:
        logger.error("Error: invalid number of dependent variables.")
        sys.exit(1)
    # Loop through each block
    i = 3  # Move counter to start after number of blocks
    for k in range(num_blocks):
        alpha_block = []
        beta_block = []
        coeff_block = []
        # Get number of alpha values (must be integer and same for every
        # block)
        if not k:
            try:
                num_alpha_values = int(table_data[i])
            except ValueError:
                logger.error("Error: invalid number of %s values in block "
                             "%d.", label_a, k + 1)
                sys.exit(1)
        else:
            try:
                n_ak = int(table_data[i])
            except ValueError:
                logger.error("Error: invalid number of %s values in block "
                             "%d.", label_a, k + 1)
                sys.exit(1)
            if n_ak is not num_alpha_values:
                logger.error("Error: number of %s values in block %d does"
                             " not match.", label_a, k + 1)
                sys.exit(1)
        i += 1
        # Get number of beta values (must be integer and same for every
        # block)
        if not k:
            try:
                num_beta_values = int(table_data[i])
            except ValueError:
                logger.error("Error: invalid number of %s values in block "
                             "%d.", label_b, k + 1)
                sys.exit(1)
        else:
            try:
                n_bk = int(table_data[i])
            except ValueError:
                logger.error("Error: invalid number of %s values in block "
                             "%d.", label_b, k + 1)
                sys.exit(1)
            if n_bk is not num_beta_values:
                logger.error("Error: number of %s values in block %d does"
                             " not match.", label_b, k + 1)
                sys.exit(1)
        i += 1
        # Save dependent variable label
        label_g.append(table_data[i])
        # Save beta values
        for beta_counter_i in range(1, num_beta_values + 1):
            beta_block.append(table_data[i + beta_counter_i])
        # Move counter to the end of the list of beta values
        i += num_beta_values + 1
        # Save alpha value and the coeff values for each row
        for _ in range(1, num_alpha_values + 1):
            alpha_block.append(table_data[i])
            coeff_row = []
            for beta_counter_j in range(1, num_beta_values + 1):
                coeff_row.append(table_data[i + beta_counter_j])
            coeff_block.append(coeff_row)
            i += num_beta_values + 1  # Move counter to next row
        # Transpose coeff list before saving to master list
        coeff_block = np.asarray(coeff_block).T.tolist()
        alpha.append(alpha_block)
        beta.append(beta_block)
        coeff.append(coeff_block)

    # Convert lists to arrays of floats
    try:
        alpha = np.transpose(np.array(alpha).astype(float))
        beta = np.transpose(np.array(beta).astype(float))
        coeff = np.transpose(np.array(coeff).astype(float))
    except ValueError as error:
        logger.error("Error: invalid table entry: %s",
                     error.args[0].split('float():')[-1].strip())
        sys.exit(1)

    return alpha, beta, coeff, label_g


def interpolate_data(collocation_points, tau_azimuths, tau_data, camrad_azimuths,
                     interpolation="Spline"):
    """ Function that interpolates camrad data to fit with the azimuth positions of tau data.
    Ensures that the shape of tau_data matches that of camrad_data. TAU data must be interpolated
    as a periodic function as CAMRAD only accepts periodic force/moment data. Two methods of
    interpolation are employed: Fourier series or Spline interpolation.

    Arguments:
        collocation_points (ndarray): Numpy array of shape (# collocation points) containing the
        collocation point data from CAMRAD.
        tau_azimuths (ndarray): Numpy array of shape (# TAU azimuth positions) containing azimuth
        angle positions for TAU output data
        tau_data (ndarray): Numpy array containing force and moment output data from TAU. Array is
        of shape (# collocation points x # TAU azimuth positions x 6). End dimension is 6
        because it stores force/moment data as (Fx, Fy, Fz, Mx, My, Mz).
        camrad_azimuths (ndarray): Numpy array of shape (# CAMRAD azimuth positions) containing
        azimuth  angle positions for CAMRAD output data
        camrad_data (ndarray): Numpy array containing force and moment output data from CAMRAD.
        Array is of shape (# collocation points x # CAMRAD azimuth positions x 6). End
        dimension is 6 because it stores force/moment data as (Fx, Fy, Fz, Mx, My, Mz).

    Returns:
        tau_data (ndarray): Interpolated TAU data of the same shape as input array camrad_data.
        Of shape (# collocation points x # CAMRAD azimuth positions x 6)
    """
    logger = logging.getLogger("camrad")
    if interpolation == "Spline":
        logger.info("Using spline interpolation to map TAU data to CAMRAD data")
        interpolation_function = spline_interpolation
    elif interpolation == "Fourier":
        logger.info("Using Fourier interpolation to map TAU data to CAMRAD data")
        interpolation_function = fourier_interpolation
    elif interpolation == "Linear":
        logger.info("Using Linear interpolation to map TAU data to CAMRAD data")
        interpolation_function = linear_interpolation
    else:
        logger.error("Interpolation function %s not found.", interpolation)
        sys.exit(1)

    num_collocation_points = collocation_points.shape[0] // 3

    interpolated_tau_data = np.zeros(tau_data.shape)

    camrad_azimuths = camrad_azimuths[:, 0]

    for panel in range(num_collocation_points):
        interpolated_data = np.zeros((tau_azimuths.shape[0], 6))
        for quantity in range(6):
            interpolated_data[:, quantity] = interpolation_function(tau_data[panel, :, quantity],
                                                                    tau_azimuths, camrad_azimuths)
        interpolated_tau_data[panel] = deepcopy(interpolated_data)

    return interpolated_tau_data


def spline_interpolation(data, x_old, x_new):
    """Function to perform spline interpolation based on input data and a given discretization.
    Function will set first and last value equal to average of these two values in given data array
    as periodic output is needed for CAMRAD. Data is interpolated using scipy.interpolate libraries

    Arguments:
        data (ndarray): 1D numpy array of input data with shape of len(x_old)
        x_old (ndarray): 1D numpy array containing original discretization
        x_new: 1D numpy array containing discretization to be interpolated

    Returns:
        data (ndarray): 1D numpy array of interpolated data based off of input data with shape of
        len(x_new)
    """

    # Output
    data[0] = data[-1] = (data[0] + data[-1]) / 2
    spl = splrep(x_old, data)

    return splev(x_new, spl)


def fourier_interpolation(data, x_old, x_new):
    """Function to perform fourier interpolation based on input data and a given discretization.
        Function will set first and last value equal to average of these two values in given data
        array as periodic output is needed for CAMRAD. Data is interpolated using numpy libraries.
        Note that this assumes that the new discretisation is equally spaced between 0 and 360.

    Arguments:
        data (ndarray): 1D numpy array of input data with shape of len(x_old)
        x_old (ndarray): 1D numpy array containing original discretization
        x_new: 1D numpy array containing discretization to be interpolated

    Returns:
        data (ndarray): 1D numpy array of interpolated data based off of input data with shape of
        len(x_new)
    """

    data[0] = data[-1] = (data[0] + data[-1]) / 2

    x_new_length = x_new.shape[0]
    x_old_length = x_old.shape[0]
    coeffs = sp.fft(data)

    nyqst = math.ceil((x_old_length + 1) / 2)
    new_coeffs = np.concatenate(
        (
            coeffs[1:nyqst], np.zeros(x_new_length - x_old_length, coeffs[nyqst + 1:x_old_length])
        )
    )

    return sp.ifft(new_coeffs)


def linear_interpolation(data, x_old, x_new):
    """Function to perform spline interpolation based on input data and a given discretization.
        Function will set first and last value equal to average of these two values in given data
        array as periodic output is needed for CAMRAD. Data is interpolated using numpy libraries

        Arguments:
            data (ndarray): 1D numpy array of input data with shape of len(x_old)
            x_old (ndarray): 1D numpy array containing original discretization
            x_new: 1D numpy array containing discretization to be interpolated

        Returns:
            data (ndarray): 1D numpy array of interpolated data based off of input data with shape
            of len(x_new)
        """

    # Output
    data[0] = data[-1] = (data[0] + data[-1]) / 2

    return np.interp(x_new, x_old, data)
