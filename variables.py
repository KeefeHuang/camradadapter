"""
@package tauadapter
Python wrapper to control the execution of TAU coupled with preCICE
"""
import logging
from os.path import join, isfile
import os
import fcntl
import json
from mpi4py import MPI


class Variables(object):
    """ Variables stores and passes variable information before preCICE interface is intialized.
    Mainly used to pass size of information apriori"""

    def __init__(self, config, write_dict):
        """ Constructor for Variables class. Takes config object to be read and updated

        Arguments:
            config (config.Config): Config object that stores coupling and simulation data
        """
        self.config = config
        self.write_dict = write_dict
        self.read_dict = None

        self.write_dict['read'] = False
        output = self.config.shared_folder
        write_file = self.config.partner + ".json"
        read_file = self.config.participant + ".json"
        self.write_file = join(output, write_file)
        self.read_file = join(output, read_file)
        self.logger = logging.getLogger("camrad.variableexchange")

    def write_data(self):
        """ Writes variables in read_data dict to yaml object in shared folder
        """
        if MPI.COMM_WORLD.Get_rank() == 0:
            self.logger.info("Writing dict to %s", self.write_file)
            if isfile(self.write_file):
                os.remove(self.write_file)
            while True:
                try:
                    with open(self.write_file, "w+") as write_file:
                        fcntl.flock(write_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
                        json.dump(self.write_dict, write_file)
                        fcntl.flock(write_file, fcntl.LOCK_UN)
                    break
                except (IOError, TypeError):
                    pass

    def read_data(self):
        """ Reads variables from yaml object in shared folder. Sets key value 'read' to True if all
        processes have read from file
        """
        rank = MPI.COMM_WORLD.Get_rank()
        while True:
            try:
                with open(self.read_file, "r+") as read_file:
                    fcntl.flock(read_file, fcntl.LOCK_SH | fcntl.LOCK_NB)
                    self.logger.debug("%d: Has lock", rank)
                    self.read_dict = json.load(read_file)
                    self.logger.info("Reading %s", self.read_file)
                    fcntl.flock(read_file, fcntl.LOCK_UN)
                    break
            except (IOError, TypeError, ValueError):
                pass

        MPI.COMM_WORLD.Barrier()
        if MPI.COMM_WORLD.Get_rank() == 0:
            while True:
                try:
                    with open(self.read_file, "r+") as read_file:
                        fcntl.flock(read_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
                        self.logger.debug("%d: Has lock", rank)
                        self.read_dict = json.load(read_file)
                        self.read_dict['read'] = True
                        read_file.seek(0)
                        json.dump(self.read_dict, read_file)
                        read_file.truncate()
                        self.logger.debug("%d: Release lock", rank)
                        fcntl.flock(read_file, fcntl.LOCK_UN | fcntl.LOCK_NB)
                        break
                except (IOError, TypeError):
                    pass

    def clear_data(self):
        """ Perform cleanup of written yaml files.
        """
        if MPI.COMM_WORLD.Get_rank() == 0:
            while True:
                try:
                    with open(self.write_file, "r") as write_file:
                        fcntl.flock(write_file, fcntl.LOCK_EX | fcntl.LOCK_NB)
                        self.read_dict = json.load(write_file)
                        if self.read_dict['read']:
                            fcntl.flock(write_file, fcntl.LOCK_UN)
                            os.remove(self.write_file)
                            self.logger.debug("Cleaning %s", self.write_file)
                            break
                        fcntl.flock(write_file, fcntl.LOCK_UN)
                except (IOError, TypeError, ValueError):
                    pass

    def update_config(self):
        """ Updates self.config with values read from yaml file. Assumes that only read_data and
        simulation variables are updated
        """
        try:
            interface_updates = self.read_dict['interfaces']
            for interface_update in interface_updates:
                value = interface_update['read_data']
                for interface in self.config.interfaces:
                    if interface['read_data'] == value:
                        for key in interface_update:
                            self.logger.debug("Updating mesh %s: %s with %s", interface.mesh,
                                              key, interface_update[key])
                            interface[key] = interface_update[key]
        except KeyError:
            pass
        try:
            simulation_updates = self.read_dict['simulation']
            for key in simulation_updates:
                self.logger.debug("Updating %s in Simulation with %s", key,
                                  simulation_updates[key])
                self.config.simulation[key] = simulation_updates[key]
        except KeyError:
            pass

    def exchange_variables(self):
        """ Method to control writing/reading of data
        """
        self.write_data()
        MPI.COMM_WORLD.Barrier()
        self.read_data()
        self.update_config()
        MPI.COMM_WORLD.Barrier()
        self.clear_data()
        MPI.COMM_WORLD.Barrier()

    def __del__(self):
        """ Destructor to ensure that config files are deleted upon sudden termination of program
        """
        if MPI.COMM_WORLD.Get_rank() == 0:
            try:
                os.remove(self.write_file)
            except OSError:
                pass
