"""
@package tauadapter
This module contains the unittests for the azimuths handlers for CAMRAD
"""
# pylint: disable=wrong-import-position
import sys
import unittest
from mock import Mock, patch

import numpy as np
CAMRAD_PATH = sys.path[0].rsplit("/", 1)[0]

sys.path.append(CAMRAD_PATH)

from azimuthshandler import AzimuthsHandler
from utility import Config


class TestAzimuthsHandler(unittest.TestCase):
    """ unittest.TestCase created to run unittests on camrad azimuthshandler.AzimuthsHandler class
    """

    # pylint: disable=arguments-differ, unused-argument
    @patch("logging.getLogger")
    def setUp(self, mock_logger):
        """ Constructor for unittests, used to create AzimuthsHandler instance for testing
        """
        data_id = mesh_id = 0
        precice_vertex_ids = [1, 2, 3]
        dimensions = 3
        self.config = Mock(spec=Config)
        self.data_handler = AzimuthsHandler(mesh_id, data_id, precice_vertex_ids, dimensions,
                                            None, self.config)

    def test_read(self):
        """ Test read method. Sample data passed, this data should be stored in Config object"""
        data = [1, 2, 3, 4, 5]

        self.data_handler.read(data, 0)

        np.testing.assert_array_equal(np.array(self.config.azimuths), np.array(data))

    def test_write(self):
        """ Test write method. This method is not implemented and should raise a
        NotImplementedError"""
        data = [1, 2, 3, 4, 5]
        with self.assertRaises(NotImplementedError):
            self.data_handler.write(data)

    def test_is_scalar(self):
        """ Test is_scalar method. This method should return true as azimuths are scalar"""

        self.assertTrue(self.data_handler.is_scalar())

    def test_get_dimensions(self):
        """ Test get_dimension method. This method should return 3"""

        self.assertEqual(self.data_handler.get_dimensions(), 3)
