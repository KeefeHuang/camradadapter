"""
@package tauadapter
This module contains the unittests for the motion data handlers for CAMRAD
"""
# pylint: disable=wrong-import-position
import sys
import unittest
from mock import Mock, patch

import numpy as np

CAMRAD_PATH = sys.path[0].rsplit("/", 1)[0]

sys.path.append(CAMRAD_PATH)
from motionshandler import MotionsHandler
from utility import Config


class TestCollocationsHandler(unittest.TestCase):
    """ unittest.TestCase created to run unittests on camrad azimuthshandler.AzimuthsHandler class
    """

    # pylint:disable=arguments-differ, unused-argument
    @patch("logging.getLogger")
    def setUp(self, mock_logger):
        """ Constructor for unittests, used to create AzimuthsHandler instance for testing
        """
        data_id = mesh_id = [0]
        precice_vertex_ids = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11]
        dimensions = 3
        self.config = Mock(spec=Config)
        config_attr = {
            "job_info_file": None,
            "output_location": "location",
            "output_prefix": "prefix"
        }
        self.config.configure_mock(**config_attr)
        self.data_handler = MotionsHandler(mesh_id, data_id, precice_vertex_ids,
                                           dimensions, None, self.config)

    def test_read(self):
        """ Test read method. This method is not implemented and should raise a
        NotImplementedError"""
        data = [1, 2, 3, 4, 5]
        with self.assertRaises(NotImplementedError):
            self.data_handler.read(data, 0)

    @patch("camrad_functions.package_motion_data", return_value=np.array([1, 2, 3, 4, 5]))
    @patch("camrad_functions.get_blade_pos_data", return_value=None)
    @patch("camrad_functions.read_camrad_data", return_value=None)
    def test_write(self, mock_read_data, mock_get_blade_pos, mock_collocations):
        """ Test write method. Sample data passed, this data should be stored in Config object"""
        sample_output = np.array([1, 2, 3, 4, 5, 0, 0, 0, 0, 0, 0])
        output = self.data_handler.write(0)
        mock_read_data.assert_called_once_with(self.config.job_info_file)
        mock_get_blade_pos.assert_called_once_with(1, None, 'location/prefix0', 1)
        mock_collocations.assert_called_once_with(None, None)
        np.testing.assert_array_equal(output, sample_output)

    def test_is_scalar(self):
        """ Test is_scalar method. This method should return true as azimuths are scalar"""

        self.assertTrue(self.data_handler.is_scalar())

    def test_get_dimensions(self):
        """ Test get_dimension method. This method should return 3"""

        self.assertEqual(self.data_handler.get_dimensions(), 3)
