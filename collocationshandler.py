"""
@package tauadapter
Python wrapper to control the execution of TAU coupled with preCICE
"""

import logging
import numpy as np

from camradadapter.datahandler import DataHandler


class CollocationsHandler(DataHandler):
    """ CollocationsHandler class. Writes collocation data from CAMRAD solver

    Attributes:
        precice_vertex_ids (list): list of unique integer ids identifying the specific vertices in
        the precice interface
        precice_vertex_ids (list): list of precice vertex_ids
        rotor_info (config.RotorInfo): jsonobject that stores rotorblade definitions
    """

    # pylint: disable=duplicate-code
    def __init__(self, mesh_id, data_id, precice_vertex_ids, config):
        """ Constructor for ForcesHandler class.

        Arguments:
            mesh_id (int): unique integer id identifying the specific mesh in precice interface
            data_id (list): unique integer id identifying the specific data type in precice
                interface
            precice_vertex_ids (ndarray): list of unique integer ids identifying the specific
                vertices in the precice interface
            config (camard_utility.Config): camrad_utility.Config object storing simulation
                parameters
        """
        super(CollocationsHandler, self).__init__(mesh_id, data_id, precice_vertex_ids)
        self.config = config
        self.simulation = config.simulation
        self.rotor_info = config.rotor_info

        self.logger = logging.getLogger("camrad.collocationshandler")
        self.logger.info("Setting up collocationshandler")

    def write(self, iteration):
        """ Implements datahandlers.DataHandler.write() abstract function.

        Arguments:
            iteration (int): current coupled iteration as int
        """
        self.logger.info("Writing collocation data")
        collocation_data, num_collocation_points = self.package_collocation_data()

        self.simulation.num_collocation_points = num_collocation_points
        return collocation_data

    def read(self, data, iteration):
        """ Reads collocation point data from preCICE. Read is not implemented for collocation
        points in CAMRAD

        Arguments:
            data (ndarray): data received from preCICE. This is in a 1D array of size (n x 3)
            iteration (int): current coupled iteration as int
        """
        self.logger.error("Read is not implemented for collocation data for this module\n")
        raise NotImplementedError

    # pylint: disable=duplicate-code
    def is_scalar(self):
        """ Function returns true if data handled is scalar. Collocation point data is scalar.
        Returns:
             Returns false as this is a vector data type
        """
        return True

    def package_collocation_data(self):
        """ Function to package collocation data for processing by Fluid solver. Data is packaged
        for sending

        Returns:
            Packaged collocation point data. Data is arranged as follows  [num collocation points] +
            [edges of panels along blade] + [collocation point coordinates]. The collocation point
            coordinates are arranged as [x0, y0, z0, x1, y1, z1...]
        """
        r_edge = self.rotor_info.redge
        r_prop = self.rotor_info.rprop
        xqc = self.rotor_info.xqc
        zqc = self.rotor_info.zqc
        radius = self.rotor_info.radius
        num_collocation_points = len(r_edge) - 1
        collocation_points = np.zeros(num_collocation_points * 3)

        for i in range(num_collocation_points):
            left_edge = r_edge[i]
            right_edge = r_edge[i + 1]

            collocation_points[i * 3 + 1] = mid_point = (left_edge + right_edge) / 2

            last_pos = 0
            for j, pos in enumerate(r_prop):
                if (mid_point - pos) < 0.001:
                    collocation_points[i * 3] = xqc[j]
                    collocation_points[i * 3 + 2] = zqc[j]
                    break

                if pos > mid_point:
                    coeff = (mid_point - last_pos) / (pos - last_pos)
                    collocation_points[i * 3] = xqc[j - 1] + coeff * (xqc[j] - xqc[j - 1])
                    collocation_points[i * 3 + 2] = zqc[j - 1] + coeff * (zqc[j] - zqc[j - 1])

                last_pos = pos

        r_edge = radius * np.array(r_edge)
        collocation_points = radius * collocation_points
        return np.concatenate((r_edge, collocation_points)), num_collocation_points
