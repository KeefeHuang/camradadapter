# 2017-05-18
"""Functions to interact with CAMRAD output files.

Provides:
    line_item_skip: read words in a line of text skipping the first n words and
        append the rest to a list
    read_data_block: read block of sensor data from SPAN STATION until MEAN
        appending to list
    read_sensor_data: read sensor data and store in array
    camrad_blade_forces_moments: get blade force and moment data from CAMRAD
        output file
    get_pos_data: collect and return sensor position data from output file in
        arrays
    read_setup_data: read general info about rotor setup from CAMRAD output
        file
    get_control_inputs: read and return control input values from CAMRAD output
        file

"""
import sys
import re
import logging
import math
import numpy as np


def line_item_skip(line, words_to_skip, data_array):
    """Read items in line skipping first n words and append to dataArray.

    Args:
        line (string): line of text to be analyzed
        words_to_skip (int): number of words to skip
        data_array (list): list for data to be appended to

    """
    i = words_to_skip
    while i < len(line.split()):
        data_array.append(line.split()[i].strip())
        i += 1


def read_data_block(data_file, sensor_data):
    """Read block of data from SPAN STATION until MEAN appending to sensorData.

    Args:
        data_file: output file
        sensor_data: list for storing data from sensor

    """
    i = 0
    row_data = []
    # Find span station data
    for line in data_file:
        if 'SPAN STATION ' in line:
            # Skip first two items ('SPAN' 'STATION') and save span stations
            line_item_skip(line, 2, row_data)
            break
    sensor_data[i] += row_data
    row_data = []
    i += 1

    # Skip line ('TIME HISTORY')
    data_file.next()

    # Read until 'HARMONICS'
    for line in data_file:
        if 'HARMONICS' in line:
            break
        # Skip first three items ('PSI' '=' 'angle') and save sensor data
        line_item_skip(line, 3, row_data)
        sensor_data[i] += row_data
        i += 1
        row_data = []

    # Read in MEAN data
    for line in data_file:
        line_item_skip(line, 1, row_data)
        break
    sensor_data[i] += row_data
    row_data = []


def read_sensor_data(sensor_name, file_name):
    """Read sensor data and store in array.

    Read the first block of sensor data and then use read_data_block function
    to read in the rest of the blocks.

    Args:
        sensor_name (string): sensor name as written in output file
        file_name (string): name of output file (including path and extension)
        log (string): name of parent logger (default: '')

    Returns:
        alpha (array): 1D vector containing radial stations (size: n_panel)
        beta (array): 1D vector containing azimuth angles (size: n_azimuth)
        coeff (array): 2D vector containing sensor data as a function of radial
            station and azimuth angle (size: len(beta) x len(alpha))
    """

    logger = logging.getLogger("camrad")

    # Open file and get data for sensor
    with open(file_name, 'r') as data_file:

        sensor_data = []
        row_data = []

        # Make file name element (1,1) (removing path)
        row_data.append(file_name.split('/')[-1])

        # Find location of last output loop
        for line in iter(data_file.readline, ''):
            if sensor_name in line:
                last = data_file.tell()

        # Move to position of last sensor occurence
        try:
            data_file.seek(last)
        except UnboundLocalError:
            logger.error("Error: sensor %s not found in output file.", sensor_name)
            sys.exit(1)

        for line in data_file:
            if 'SPAN STATION ' in line:
                # Skip first two items ('SPAN' 'STATION') and save span
                # stations
                line_item_skip(line, 2, row_data)
                break

        # Save row to master list
        sensor_data.append(row_data)
        # Reset row data list
        row_data = []

        # Skip line ('TIME HISTORY')
        data_file.next()

        # Read until 'HARMONICS'
        for line in data_file:
            if 'HARMONICS' in line:
                break
            # Skip first two items ('PSI' '=') and save sensor data
            line_item_skip(line, 2, row_data)
            sensor_data.append(row_data)
            row_data = []

        # Read in MEAN data
        row_data.append(None)  # Placeholder so other data is aligned
        for line in data_file:
            line_item_skip(line, 1, row_data)
            break
        sensor_data.append(row_data)

        # Read data until integral/average values (string will end the loop)
        while True:
            try:
                float(sensor_data[0][-1])
                read_data_block(data_file, sensor_data)
            except ValueError:
                break

    # Convert data to numpy array
    sensor_data = np.array(sensor_data)

    # Separate data and convert to float
    # Get azimuth positions from first column removing first and last elements
    # (file name and mean value)
    beta = sensor_data[1:-1, 0].astype(float)

    # Get radial stations from first row removing first and last elements
    # file name and INTEGRAL/AVERAGE placeholder)
    alpha = sensor_data[0, 1:-1].astype(float)

    # Get coefficient data by removing first and last rows and columns
    # (radial stations, azimuth angles, mean, average/integral column)
    coeff = sensor_data[1:-1, 1:-1].astype(float)

    return alpha, beta, coeff


def camrad_blade_forces_moments(file_name):
    """Get blade force and moment data from CAMRAD output file.

    Args:
        file_name (string): name of output file (including path and extension)
        log (string): name of parent logger (default: '')

    Parameters:
        sensorNames (list): sensor names (forces and moments at the collocation
            points for all three axes)

    Returns:
        rStations (array): radial stations for sensor data (size: n_panel x 6)
        azimuth (array): azimuth angles (size: n_azimuth x 6)
        fmValues (array): sensor data as a function of radials stations and
                    azimuth angles (size: n_panel x n_azimuth x 6)

    Notes:
    OPLOAD must be set to 1 (in Rotor class, Aerodynamics type) to use forces
    and moments. Otherwise forces at the quarter and three-quarter chord will
    be used. Sensor 84 should be used to output forces and moments.

    """

    logger = logging.getLogger("camrad")

    logger.info('Collecting sensor data from CAMRAD output file: %s', file_name)

    # Use to collect dimensional force and moment data at collocation points
    # (OPLOAD must be set to 1)
    sensor_names = [("FORCE AT QUARTER CHORD COLLOCATION POINT; X COMPONENT, WING FRAME AXES"),
                    ("FORCE AT QUARTER CHORD COLLOCATION POINT; Y COMPONENT, WING FRAME AXES"),
                    ("FORCE AT QUARTER CHORD COLLOCATION POINT; Z COMPONENT, WING FRAME AXES"),
                    ("MOMENT AT QUARTER CHORD COLLOCATION POINT; X COMPONENT, WING FRAME AXES"),
                    ("MOMENT AT QUARTER CHORD COLLOCATION POINT; Y COMPONENT, WING FRAME AXES"),
                    ("MOMENT AT QUARTER CHORD COLLOCATION POINT; Z COMPONENT, WING FRAME AXES")]

    r_stations = []
    azimuth = []
    fm_values = []

    # Get sensor data from CAMRAD output file for each sensor in list
    for sensor in sensor_names:
        logger.debug('Gathering data from sensor: %s', sensor)
        data = read_sensor_data(sensor, file_name)
        r_stations.append(data[0])
        azimuth.append(data[1])
        fm_values.append(data[2])
    # Transform lists for the correct format for a table
    r_stations = np.transpose(np.array(r_stations))
    azimuth = np.transpose(np.array(azimuth))
    fm_values = np.transpose(np.array(fm_values))

    return r_stations, azimuth, fm_values


def get_blade_pos_data(blade, rotor_info, data_file, num_harmonics=1):
    """Collect and return sensor position data from output file in arrays.
    Args:
        blade (int): blade number being tracked
        rotor_info (dict): Dict containing data on the rotor
        data_file (string): output file name (including path and extension)
        num_harmonics (int): number of harmonics to track. set to 1 by default

    Returns:
        flap (array): flap data (len: num_harmonics*2 + 1) [deg]
        lag (array): lag data (len: num_harmonics*2 + 1) [deg]
        pitch (array): pitch data (len: num_harmonics*2 + 1) [deg]
        axial (array): axial data (len: num_harmonics*2 + 1) [deg]

    Notes:
    Data is formatted as [mean, cos1, sin1, cos2, sin2, ...]. This function
    works for any number of harmonics; however, the other scripts have only
    been tested for one. Check output if using more than one.
    """
    logger = logging.getLogger("camrad")

    with open(data_file, 'r') as output_data:
        data = output_data.read()

    harmonic_pattern = re.compile(
        r"((?:\s*MEAN|\s*COSINE\s*[0-9]+|\s+SINE\s*[0-9]+)\s*([0-9\.\-E]+)\s*([0-9\.\-E]+)\s*"
        r"([0-9\.\-E]+)\s*([0-9\.\-E]+))")
    radial_positions = np.array(rotor_info.rpos)
    radial_positions = np.append(radial_positions, [1.0])
    #harmonics_data = [2 * num_harmonics + 1]
    harmonics_data = []

    for radial_position in radial_positions:
        logger.debug("Storing data for RPOS: %f", radial_position)
        # locate region in camrad output file that matches this radial position
        position_pattern = re.compile(
            r"BLADE {} POS\s*{:.4f}R".format(blade, radial_position))

        start_position = re.search(position_pattern, data).start(0)
        match_objects = re.finditer(harmonic_pattern, data[start_position:])
        flap = []
        lag = []
        pitch = []
        axial = []
        match_data = next(match_objects)
        for _ in range(num_harmonics * 2 + 1):
            match_data = next(match_objects)
            flap.append(float(match_data.group(2)))
            lag.append(float(match_data.group(3)))
            pitch.append(float(match_data.group(4)))
            axial.append(float(match_data.group(5)))

        flap = [math.degrees(math.atan(x / radial_position)) for x in flap]
        lag = [math.degrees(math.atan(x / radial_position)) for x in lag]
        axial = [math.degrees(math.atan(x / radial_position)) for x in axial]

        harmonics_data += flap + lag + pitch + axial

    return harmonics_data


def read_camrad_data(file_name, config):  # pylint: disable=too-many-locals,too-many-statements
    """Read general info about rotor setup from CAMRAD output file.
    Args:
        file_name (string): file name (including path and extension) of CAMRAD
            output file
        config (config.Config): Config jsonobject used to store input parameters and pass data
            between simulation components.


    Returns:
        outDict (dict): general rotor data containing the following variables
        - R (float): rotor blade radius [m]
        - omega (float): rotational rate [rad/sec]
        - ashaft (float): rotor shaft angle of attack [deg]
        - acant (float): rotor cant (roll) angle [deg]
        - bladeSpacing (list): azimuth angles (in degrees) of each blade in
            rotor
        - rEdge (list): radial coordinates of blade panel edges [Npanel + 1]
        - xqc (list): x-axis quarter chord offset from reference line [m]
        - zqc (list): z-axis quarter chord offset from reference line [m]
        - rProp (list): radial stations of XQC and ZQC properties

    Note:
    xqc and zqc are redimensionalized by multiplying by the rotor radius.

    """
    logger = logging.getLogger("camrad.configreader")

    rotor_info = config.rotor_info

    radius_pattern = re.compile(r"RADIUS\s*=\s*([0-9\.]+).*,")
    nblade_pattern = re.compile(r"NBLADE\s*=\s*([0-9\.]+).*,")
    omega_pattern = re.compile(r"VTIPN\s*=\s*([0-9\.]+).*,")
    ashaft_pattern = re.compile(r"ASHAFT\s*=\s*([0-9\.]+).*,")
    acant_pattern = re.compile(r"ACANT\s*=\s*([0-9\.]+).*,")
    rprop_pattern = re.compile(r"RPROP\s*=\s*([0-9\.,\n\s]+),")
    xqc_pattern = re.compile(r"XQC\s*=\s*([0-9\.,\s\n]+|[0-9\.\*]+),")
    zqc_pattern = re.compile(r"ZQC\s*=\s*([0-9\.,\s\n]+|[0-9\.\*]+),")
    opaz_pattern = re.compile(r"OPAZ\s*=\s*([0-9\.\*]+).*,")
    redge_pattern = re.compile(r"REDGE\s*=\s*([0-9\.,\n\s]+),")
    rpos_pattern = re.compile(r"\s+RPOS\s*=\s*([0-9\.,\n\s]+),")

    with open(file_name, "r") as file_handle:
        file_data = file_handle.read()
    try:
        rotor_info.radius = float(re.search(radius_pattern, file_data).group(1))
    except AttributeError:
        logger.exception("RADIUS not found in %s.", file_name, exc_info=1)
        sys.exit()

    try:
        rotor_info.nblade = int(re.search(nblade_pattern, file_data).group(1))
    except AttributeError:
        logger.exception("NBLADE not found in %s.", file_name, exc_info=1)
        sys.exit()

    try:
        rotor_info.omega = float(re.search(omega_pattern, file_data).group(1)) / rotor_info.radius
    except AttributeError:
        logger.exception("VTIPN (tip velocity) not found in %s.", file_name, exc_info=1)
        sys.exit()

    try:
        rotor_info.ashaft = float(re.search(ashaft_pattern, file_data).group(1))
    except AttributeError:
        logger.info("ASHAFT not found in %s, setting to value to 0 by default", file_name)
        rotor_info.ashaft = 0

    try:
        rotor_info.acant = float(re.search(acant_pattern, file_data).group(1))
    except AttributeError:
        logger.info(
            "ACANT not found in %s, setting to value to 0 by default", file_name)
        rotor_info.acant = 0

    try:
        raw_rprop = re.search(rprop_pattern, file_data).group(1).split(",")
        rprop = []
        for prop in raw_rprop:
            try:
                new_prop = float(prop)
                rprop.append(new_prop)
            except TypeError:
                continue
        rotor_info.rprop = rprop
    except AttributeError:
        logger.exception("RPROP not found in %s.", file_name, exc_info=1)
        sys.exit()

    try:
        xqc_search = re.search(xqc_pattern, file_data).group(1)
        try:
            xqc_list = xqc_search.split(",")
            rotor_info.xqc = list(map(float, xqc_list))
        except ValueError:
            logger.info("XQC values found as %s, converting to list", xqc_search)
            xqc_length = int(xqc_search.split("*")[0])
            xqc_value = float(xqc_search.split("*")[1])
            rotor_info.xqc = list(xqc_length * [xqc_value])
    except AttributeError:
        logger.exception("XQC not found in %s.", file_name, exc_info=1)
        sys.exit()

    try:
        zqc_search = re.search(zqc_pattern, file_data).group(1)
        try:
            zqc_list = zqc_search.split(",")
            rotor_info.zqc = list(map(float, zqc_list))
        except ValueError:
            logger.info("ZQC values found as %s, converting to list", zqc_search)
            zqc_length = int(zqc_search.split("*")[0])
            zqc_value = float(zqc_search.split("*")[1])
            rotor_info.zqc = list(zqc_length * [zqc_value])
    except AttributeError:
        logger.exception("ZQC not found in %s.", file_name)
        sys.exit()

    try:
        rotor_info.opaz = int((re.search(opaz_pattern, file_data).group(1)))
    except AttributeError:
        rotor_info.opaz = 0
        blade_spacing = []
        for i in range(rotor_info.nblade):
            blade_spacing.append(i * float(360) / rotor_info.nblade)
        rotor_info.blade_spacing = blade_spacing

    try:
        raw_redge = re.search(redge_pattern, file_data).group(1).split(",")
        redge = []
        for edge in raw_redge:
            try:
                new_edge = float(edge)
                redge.append(new_edge)
            except TypeError:
                continue
        rotor_info.redge = redge
    except AttributeError:
        logger.exception("REDGE not found in %s.", file_name, exc_info=1)
        sys.exit()

    try:
        raw_rpos = re.search(rpos_pattern, file_data).group(1).split(",")
        rpos = []
        for pos in raw_rpos:
            try:
                new_pos = float(pos)
                rpos.append(new_pos)
            except TypeError:
                continue
        rotor_info.rpos = rpos
    except AttributeError:
        logger.exception("RPOS not found in %s.", file_name, exc_info=1)
        sys.exit()


def get_control_inputs(file_name):
    """Read and return control input values from CAMRAD output file.

    Args:
        file_name (string): file name (including path and extension) of CAMRAD
            output file
        log (string): name of parent logger (default: '')

    Returns:
        coll (float): rotor collective input [deg]
        latcyc (float): rotor lateral cyclic input [deg]
        longcyc (float): rotor longitudinal cyclic input [deg]

    """

    logger = logging.getLogger("camrad")

    # Open file and get data for sensor
    with open(file_name, 'r') as data_file:

        # Find location of last output loop
        for line in iter(data_file.readline, ''):
            if 'PILOT CONTROL (DEG)' in line:
                last = data_file.tell()

        # Move to position of last sensor occurence
        try:
            data_file.seek(last)
        except UnboundLocalError:
            logger.error("Error: control inputs not found in output file.")
            sys.exit(1)

        for line in data_file:
            if 'COLLECTIVE' in line:
                coll = float(line.split()[2])
                latcyc = float(data_file.next().split()[3])
                longcyc = float(data_file.next().split()[3])
                break

    return [coll, latcyc, longcyc]


def package_collocation_data(rotor_info):
    """ Function to package collocation data for processing by Fluid solver. Data is packaged
    for sending

    Arguments:
        rotor_info (config.RotorInfo): RotorInfo jsonobject. stores camrad rotorblade definition
            information

    Returns:
        Packaged collocation point data. Data is arranged as follows  [num collocation points] +
        [edges of panels along blade] + [collocation point coordinates]. The collocation point
        coordinates are arranged as [x0, y0, z0, x1, y1, z1...]
    """
    r_edge = rotor_info.redge
    r_prop = rotor_info.rprop
    xqc = rotor_info.xqc
    zqc = rotor_info.zqc
    radius = rotor_info.radius
    num_collocation_points = len(r_edge) - 1
    collocation_points = np.zeros(num_collocation_points * 3)

    for i in range(num_collocation_points):
        left_edge = r_edge[i]
        right_edge = r_edge[i + 1]

        collocation_points[i * 3 + 1] = mid_point = (left_edge + right_edge) / 2

        last_pos = 0
        for j, pos in enumerate(r_prop):
            if (mid_point - pos) < 0.001:
                collocation_points[i * 3] = xqc[j]
                collocation_points[i * 3 + 2] = zqc[j]
                break

            if pos > mid_point:
                coeff = (mid_point - last_pos) / (pos - last_pos)
                collocation_points[i * 3] = xqc[j - 1] + coeff * (xqc[j] - xqc[j - 1])
                collocation_points[i * 3 + 2] = zqc[j - 1] + coeff * (zqc[j] - zqc[j - 1])

            last_pos = pos

    collocation_points = radius * collocation_points
    return collocation_points


def create_intermediate_mesh(rotor_info, config):
    """ Function to create an intermediate mesh between 3D fluid solver and 1D structure mesh in
    CAMRAD. A relatively uniform mesh is created by determining a standard element size (lengthwise
    and widthwise). Element size lengthwise (in direction of hub to wingtip) is determined by
    finding the average panel length and dividing by the number of lengthwise elements provided in
    config file. Element size widthwise (perpendicular to lengthwise direction and in-plane with
    blade) is determined by dividing blade_width by number of widthwise elements, both provided in
    the config file. Blade width is assumed to be symmetrical and centered at 0, i.e. a blade_width
     of 0.1 is interpreted as a range of [-0.05, 0.05]

    Arguments:
        rotor_info (config.RotorInfo): Dictionary containing rotor information. Minimally contains
        panel edge
        (rEdge) and radius length (R)
        config (Config): Config jsonobject created by camrad_utility.configure() function.

    Returns:
        Tuple of two lists.
        - intermediate_mesh (list): 2D grid with coordinates of nodes in intermediate mesh. Will be
            passed to preCICE to set intermediate mesh
        - intermediate_mesh_collocation_point_ids (list): 2D grid of same dimensions as
            intermediate_mesh, contains index of collocation point each node in intermediate mesh
            belongs to. Used to easily integrate/aggregate force/moment data after passing through
            preCICE.
    """
    simulation = config.simulation
    r_edges = np.array(rotor_info.redge) * rotor_info.radius

    # Get panel widths using edges of panels
    panel_widths = r_edges[1:] - r_edges[:-1]

    # Determine average lengthwise element size. Divide all panel_widths by number of lengthwise
    # elements defined in config file. Find average of these values.
    lengthwise_elements = simulation.lengthwise_elements
    widthwise_elements = simulation.widthwise_elements
    blade_width = simulation.blade_width
    # Widthwise range locations calculated using blade width and number of widthwise elements
    # defined in config file
    widthwise_range = np.linspace(config.blade_width, -config.blade_width,
                                  num=config.widthwise_elements)

    intermediate_mesh = None
    intermediate_mesh_collocation_point_ids = None

    for i, _ in enumerate(panel_widths):
        # lengthwise_element_size = panel_width / lengthwise_elements
        # Determine number of elements (int value) in this panel based on average element size
        # num_elements = int(round(panel_width / lengthwise_element_size))
        # Distance between each node in this panel
        average_length = 0.005
        # Lengthwise range locations. Note that nodes are not placed on rEdge values so allocation
        # of nodes to panels in CAMRAD is clear.
        lengthwise_range = np.linspace(r_edges[i] + average_length / 2,
                                       r_edges[i + 1] - average_length / 2,
                                       num=lengthwise_elements)

        # Create a np array of shape (w x l x 1) using widthwise and lengthwise values
        grid = np.array(
            [[x, y, 0.0] for x in widthwise_range for y in lengthwise_range])

        grid = grid.flatten()
        grid_ids = np.full((grid.shape[0] // 3), i)

        # Aggregate values over multiple panels
        if intermediate_mesh is None:
            intermediate_mesh = grid
            intermediate_mesh_collocation_point_ids = grid_ids
        else:
            intermediate_mesh = np.concatenate((intermediate_mesh, grid))
            intermediate_mesh_collocation_point_ids = np.concatenate(
                (intermediate_mesh_collocation_point_ids, grid_ids))

    simulation.intermediate_mesh = list(intermediate_mesh)
    simulation.intermediate_mesh_collocation_point_ids = list(
        intermediate_mesh_collocation_point_ids)
    return intermediate_mesh, intermediate_mesh_collocation_point_ids
